from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy

from core.models import Seller
from .forms import CreateSellerForm
# Create your views here.
##### S E L L E R C R U D  #####

# Create
class CreateSeller(generic.CreateView):
    template_name = "core/create_seller.html"
    model = Seller
    form_class = CreateSellerForm
    success_url = reverse_lazy("core:list_seller")
    

# Retrieve
class ListSeller(generic.View):
    template_name = "core/list_seller.html"
    context = {}

    def get(self, request):
        self.context = {
            "sellers": Seller.objects.filter(status=True)
        }
        return render(request, self.template_name, self.context)
    
class DetailSeller(generic.View):
    template_name = "core/detail_seller.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "seller": Seller.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)

# Update

# Delete