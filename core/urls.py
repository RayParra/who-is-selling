from django.urls import path

from core import views

app_name = "core"


urlpatterns = [
     ##### SELLERS URLS #####
    path('seller/list/', views.ListSeller.as_view(), name="list_seller"),
    path('seller/detail/<int:pk>/', views.DetailSeller.as_view(), name="detail_seller"),
    path('seller/create/', views.CreateSeller.as_view(), name="create_seller"),
]