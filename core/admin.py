from django.contrib import admin

# Register your models here.
from core import models


@admin.register(models.Seller)
class SellerAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "timestamp",
        "status",
    ]


@admin.register(models.Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "seller",
        "follow_up",
        "status",
    ]


@admin.register(models.ProductCategory)
class ProductCategoryAdmin(admin.ModelAdmin):
    list_display = [
        "product_category_name",
        "timestamp",
        "status",
    ]


@admin.register(models.ProductSubCategory)
class ProductSubCategoryAdmin(admin.ModelAdmin):
    list_display = [
        "product_subcategory_name",
        "timestamp",
        "status",
    ]


@admin.register(models.Provider)
class ProviderAdmin(admin.ModelAdmin):
    list_display = [
        "provider_name",
        "static_phone",
        "status",
    ]


@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = [
        "product_name",
        "provider",
        "status",
    ]


@admin.register(models.ServiceCategory)
class ServiceCategoryAdmin(admin.ModelAdmin):
    list_display = [
        "service_category_name",
        "timestamp",
        "status",
    ]


@admin.register(models.ServiceSubCategory)
class ServiceSubCategoryAdmin(admin.ModelAdmin):
    list_display = [
        "service_subcategory_name",
        "timestamp",
        "status",
    ]


@admin.register(models.Service)
class ServiceAdmmin(admin.ModelAdmin):
    list_display = [
        "service_name",
        "provider",
        "status",
    ]