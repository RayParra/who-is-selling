from django import forms

from .models import Seller

class CreateSellerForm(forms.ModelForm):
    class Meta:
        model = Seller
        fields = [
            "user",
            "name",
            "certification",
            "image",
            "bio",
            "status",
            "slug"
        ]
        widgets = {
            "user": forms.Select(attrs={"type":"select", "class":"form-select"}),
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe Nombre del Seller"}),
            "certification": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe las Certificaciones del Seller"}),
            "image": forms.FileInput(attrs={"type":"file", "class":"form-control"}),
            "bio": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":3}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox", "class":"form-check-input"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control"})
        }