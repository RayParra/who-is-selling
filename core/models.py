from django.db import models
from django.contrib.auth.models import User
# Create your models here.


STATE_SALE = (
    ("Doing", "DG"),
    ("Done", "DN"),
    ("Pending", "PN"),
)

MONEY_TYPE = (
    ("Dolar USA", "DL"),
    ("Peso Mexicano", "MX"),
)

#Vendedor
class Seller(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=32, default="General Seller")
    certification = models.CharField(max_length=128, default="Generic Certification")
    timestamp = models.DateField(auto_now_add=True, auto_now=False)
    updated = models.DateField(auto_now_add=False, auto_now=True)
    image = models.ImageField(upload_to='images/sellers/', default='general_selller.jpg', blank=True, null=True)
    bio = models.CharField(max_length=256, default="I Love ACI")
    status = models.BooleanField(default=True)
    slug = models.SlugField()

    def __str__(self):
        return self.name 


#Cliente
class Client(models.Model):
    user = models.ForeignKey(User, models.CASCADE)
    name = models.CharField(max_length=128, default="General Public")
    web_page = models.URLField(default="https://www.genericwebpage.com", blank=True, null=True)
    email = models.EmailField(default="genericemal@generic.com", blank=True, null=True)
    static_phone = models.CharField(max_length=13, default="1234567890", blank=True, null=True)
    celphone = models.CharField(max_length=10, default="1234567890")
    image = models.ImageField(upload_to='images/clients/', default='general_client.jpg', blank=True, null=True) 
    address = models.CharField(max_length=128, default="Tijuana BC", blank=True, null=True)
    visit_date = models.DateField(auto_now_add=False, auto_now=False, blank=True, null=True)
    status = models.BooleanField(default=False)
    seller = models.ForeignKey(Seller, on_delete=models.CASCADE)
    follow_up = models.BooleanField(default=False)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.name


# Product Category
class ProductCategory(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product_category_name = models.CharField(max_length=32, default="Generic Product Category", unique=True, blank=False, null=False)
    description = models.CharField(max_length=256, default="Generic Product Category Description")
    timestamp = models.DateField(auto_now_add=True, auto_now=False)
    updated = models.DateField(auto_now_add=False, auto_now=True)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.product_category_name


# Product Subcategory
class ProductSubCategory(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product_category = models.ForeignKey(ProductCategory, on_delete=models.CASCADE)
    product_subcategory_name = models.CharField(max_length=32, default="Generic Product SubCategory", unique=True, blank=False, null=False)
    description = models.CharField(max_length=256, default="Generic Product SubCategory Description")
    timestamp = models.DateField(auto_now_add=True, auto_now=False)
    updated = models.DateField(auto_now_add=False, auto_now=True)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.product_subcategory_name


#Provider
class Provider(models.Model):
    user = models.ForeignKey(User, models.CASCADE)
    provider_name = models.CharField(max_length=32, default="Generic Provider")
    web_page = models.URLField(default="https://www.genericwebpage.com", blank=True, null=True)
    email = models.EmailField(default="genericemal@generic.com", blank=True, null=True)
    static_phone = models.CharField(max_length=13, default="1234567890", blank=True, null=True)
    image = models.ImageField(upload_to='images/clients/', default='general_client.jpg', blank=True, null=True) 
    address = models.CharField(max_length=128, default="Tijuana BC", blank=True, null=True)
    status = models.BooleanField(default=False)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.provider_name
    

#Producto

class Product(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product_name = models.CharField(max_length=32, default="Generic Product")
    description = models.CharField(max_length=254)
    stock = models.IntegerField(default=0)
    price = models.FloatField(default=0.0)
    money_type = models.CharField(max_length=16, choices=MONEY_TYPE)
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.product_name



# Service Category
class ServiceCategory(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    service_category_name = models.CharField(max_length=32, default="Generic Service Category", unique=True, blank=False, null=False)
    description = models.CharField(max_length=256, default="Generic Product Service Category Description")
    timestamp = models.DateField(auto_now_add=True, auto_now=False)
    updated = models.DateField(auto_now_add=False, auto_now=True)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.service_category_name


#Service Subcategory
class ServiceSubCategory(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    service_category = models.ForeignKey(ServiceCategory, on_delete=models.CASCADE)
    service_subcategory_name = models.CharField(max_length=32, default="Generic Service SubCategory", unique=True, blank=False, null=False)
    description = models.CharField(max_length=256, default="Generic Product Service SubCategory Description")
    timestamp = models.DateField(auto_now_add=True, auto_now=False)
    updated = models.DateField(auto_now_add=False, auto_now=True)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.service_subcategory_name


#Servicio
class Service(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    service_name = models.CharField(max_length=32, default="Generic Service")
    description = models.CharField(max_length=254)
    stock = models.IntegerField(default=0)
    price = models.FloatField(default=0.0)
    money_type = models.CharField(max_length=16, choices=MONEY_TYPE)
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.service_name

#Venta

class Venta(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    sell_code = models.CharField(max_length=64, default="Generic Sale by Generic Seller")
    seller = models.ForeignKey(Seller, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    products = models.ManyToManyField(Product, blank=True, related_name="products")
    services = models.ManyToManyField(Service, blank=True, related_name="services")
    total_sale = models.FloatField(default=0.0)
    money_type = models.CharField(max_length=16, choices=MONEY_TYPE)
    timestamp = models.DateField(auto_now_add=True, auto_now=False)
    updated = models.DateField(auto_now_add=False, auto_now=True)
    state = models.CharField(max_length=16, choices=STATE_SALE)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=16)


    def __str__(self):
        return self.sale_code