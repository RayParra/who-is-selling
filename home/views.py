from django.shortcuts import render
from django.views import generic

from core.models import Seller

# Create your views here.


class Index(generic.View):
    template_name = "home/index.html"
    context = {}

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, self.context)


